@extends('layout.utama')

@section('title')
    Detail Hukuman Disiplin
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="mb-3">
                <a href="{{ route('pegawaihukuman.index') }}" class="btn btn-sm btn-outline-secondary"><i
                        class="fa-solid fa-chevron-left"></i> Kembali</a>
            </div>
            <table class="table table-detail">
                <tbody>
                    <tr>
                        <th width="190px">Nama Pegawai</th>
                        <td>{{ $hukuman->nama_lengkap }}</td>
                    </tr>
                    <tr>
                        <th>NIP</th>
                        <td>{{ $hukuman->nip }}</td>
                    </tr>
                    <tr>
                        <th>Nama Hukuman</th>
                        <td>{{ $hukuman->nama_hukuman }}</td>
                    </tr>
                    <tr>
                        <th>Kategori Hukuman</th>
                        <td>{{ $hukuman->kategori_hukuman }}</td>
                    </tr>
                    <tr>
                        <th>TMT Awal</th>
                        <td>{{ AppHelper::showTanggal($hukuman->tmt_awal) }}</td>
                    </tr>
                    <tr>
                        <th>TMT Akhir</th>
                        <td>{{ AppHelper::showTanggal($hukuman->tmt_akhir) }}</td>
                    </tr>
                    <tr>
                        <th>Nomor SK</th>
                        <td>{{ $hukuman->nomor_sk }}</td>
                    </tr>
                    <tr>
                        <th>Tanggal SK</th>
                        <td>{{ AppHelper::showTanggal($hukuman->tanggal_sk) }}</td>
                    </tr>
                    <tr>
                        <th>Penanda tangan SK</th>
                        <td>{{ $hukuman->penandatangan_sk }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
