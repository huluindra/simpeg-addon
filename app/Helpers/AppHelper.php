<?php

namespace App\Helpers;

use Carbon\Carbon;

class AppHelper
{
    static function showTanggal($tanggal)
    {
        if (is_null($tanggal)) return '';
        $tgl = Carbon::parse($tanggal)->locale('id_ID');
        return $tgl->isoFormat('DD-MMM-YYYY');
    } // function showTanggal()
}
