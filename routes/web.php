<?php

use App\Http\Controllers\PegawaiHukumanController;
use App\Http\Controllers\PegawaiPensiunController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('pegawaipensiun.index');
});

Route::controller(PegawaiPensiunController::class)->group(function () {
    Route::get('/pegawaipensiun', 'index')->name('pegawaipensiun.index');
});

Route::controller(PegawaiHukumanController::class)->group(function () {
    Route::get('/pegawaihukuman', 'index')->name('pegawaihukuman.index');
    Route::get('/pegawaihukuman/detail/{id}', 'detail')->name('pegawaihukuman.detail');
});
