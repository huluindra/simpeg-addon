function setDefaultScroll() {
    var e = $('.default-scroll');
    if (e.length) {
        var offset = e.offset();
        $('html, body').animate({ scrollTop: offset.top });
    };
};

function setDefaultFocus() {
    var e = $('.default-focus');
    if (e.length) {
        e.focus();
    };
};

$(function () {
    setDefaultScroll();
    setDefaultFocus();
});

// berapa milisecond sweetdelete otomatis ditutup?
var __sweetAlertAutoCloseTimer = 2000;

// untuk dipanggil sweetdelete setelah berhasil menghapus data
function reloadPage() {
    location.reload();
};

function sweetDeleteData(e) {
    Swal.fire({
        text: 'Yakin untuk menghapus data ini?',
        icon: 'question',
        showConfirmButton: true,
        confirmButtonText: 'Hapus',
        focusConfirm: false,
        showCancelButton: true,
        cancelButtonText: 'Batal',
        focusCancel: true
    }).then((result) => {
        if (result.isConfirmed) {
            var url = $(e).data('deleteurl');
            var token = $('[name=_token]').val();
            $.ajax({
                type: 'POST',
                url: url,
                data: { _token: token },
                success: function (resultData) {
                    if (resultData.status) {
                        Swal.fire({
                            text: 'Data berhasil dihapus.',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: __sweetAlertAutoCloseTimer,
                            timerProgressBar: true
                        });
                        window.setTimeout(reloadPage, __sweetAlertAutoCloseTimer - 500);
                    } else {
                        Swal.fire({
                            title: 'Gagal menghapus',
                            text: resultData.desc,
                            icon: 'error',
                            showConfirmButton: true,
                        });
                    };
                },
                error: function () {
                    Swal.fire({
                        title: 'Error',
                        text: 'Gagal menghapus data.',
                        icon: 'error',
                        showConfirmButton: false,
                        timer: __sweetAlertAutoCloseTimer,
                        timerProgressBar: true
                    });
                }
            });
        };
    });
};
