<?php

return [
    'pagination_row_count' => env('BKPSDM_PAGINATION_ROW_COUNT', 50),
];
