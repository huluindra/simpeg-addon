@extends('layout.utama')

@php
$rowCount = 1;
@endphp

@section('title')
    Daftar PNS akan Pensiun
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="mb-3">

                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-primary">
                        Dalam
                        @switch($bulan)
                            @case(1)
                                1 bulan
                            @break

                            @case(3)
                                3 bulan
                            @break

                            @case(6)
                                6 bulan
                            @break

                            @case(12)
                                1 tahun
                            @break

                            @case(24)
                                2 tahun
                            @break

                            @case(36)
                                3 tahun
                            @break

                            @case(48)
                                4 tahun
                            @break

                            @case(60)
                                5 tahun
                            @break
                        @endswitch
                    </button>
                    <button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 1]) }}">1 bulan</a>
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 3]) }}">3 bulan</a>
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 6]) }}">6 bulan</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 12]) }}">1 tahun</a>
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 24]) }}">2 tahun</a>
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 36]) }}">3 tahun</a>
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 48]) }}">4 tahun</a>
                        <a class="dropdown-item" href="{{ route('pegawaipensiun.index', ['bulan' => 60]) }}">5 tahun</a>
                    </div>
                </div>

                {{-- <a href="{{ route('pegawaipensiun.index', ['bulan' => 1]) }}"
                    class="btn btn-sm @if ($bulan == 1) btn-primary @else btn-outline-primary @endif">1
                    Bulan</a>
                <a href="{{ route('pegawaipensiun.index', ['bulan' => 3]) }}"
                    class="btn btn-sm @if ($bulan == 3) btn-primary @else btn-outline-primary @endif">3
                    Bulan</a>
                <a href="{{ route('pegawaipensiun.index', ['bulan' => 6]) }}"
                    class="btn btn-sm @if ($bulan == 6) btn-primary @else btn-outline-primary @endif">6
                    Bulan</a>
                <a href="{{ route('pegawaipensiun.index', ['bulan' => 12]) }}"
                    class="btn btn-sm @if ($bulan == 12) btn-primary @else btn-outline-primary @endif">12
                    Bulan</a> --}}
                <span class="text-muted btn btn-sm" disabled="true">
                    Jumlah: {{ count($listPensiun) }} data.
                </span>
            </div>
            <div class="table-responsive">
                <table class="table table-index">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIP</th>
                            <th>@sortablelink('nama_lengkap', 'Nama Lengkap')</th>
                            <th>@sortablelink('tanggal_lahir', 'Tgl Lahir')</th>
                            <th>@sortablelink('umur', 'Umur')</th>
                            <th>@sortablelink('jenis_jabatan_id', 'Jenis Jabatan')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (!count($listPensiun))
                            <tr>
                                <td colspan="5" class="bg-warning">[tidak ada data]</td>
                            </tr>
                        @else
                            @foreach ($listPensiun as $index => $item)
                                <tr>
                                    <td>{{ $rowCount }}</td>
                                    <td>{{ $item->nip }}</td>
                                    <td>{{ $item->nama_lengkap }}</td>
                                    <td>{{ $item->tanggal_lahir }}</td>
                                    <td>{{ $item->umur }}</td>
                                    <td>{{ $item->jenis_jabatan_id }} - {{ $item->nama_jenis_jabatan }}</td>
                                </tr>
                                @php
                                    $rowCount += 1;
                                @endphp
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
