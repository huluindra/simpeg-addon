<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PegawaiPensiunViewModel extends Model
{
    use Sortable;

    protected $table = 'view_pegawai_jenis_jabatan';

    public function getUmurAttribute()
    {
        $tglLahir = Carbon::parse($this->tanggal_lahir);
        return $tglLahir->age;
    }
}
