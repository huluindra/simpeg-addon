<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisHukumanModel extends Model
{
    protected $table = 'tbl_ref_hukuman_disiplin';
}
