<?php

namespace App\Http\Controllers;

use App\Models\JenisHukumanModel;
use App\Models\PegawaiHukumanViewModel;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;

class PegawaiHukumanController extends Controller
{
    public function index(Request $request)
    {
        $cari = $request->query('cari');
        if (is_null($cari)) $cari = ['nama' => null, 'nip' => null, 'jenishukuman' => null];
        $listHukumanQuery = PegawaiHukumanViewModel::query();
        if (!is_null($cari['nama'])) $listHukumanQuery = $listHukumanQuery->where('nama_lengkap', 'LIKE', "%{$cari['nama']}%");
        if (!is_null($cari['nip'])) $listHukumanQuery = $listHukumanQuery->where('nip', 'LIKE', "%{$cari['nip']}%");
        if (!is_null($cari['jenishukuman'])) $listHukumanQuery = $listHukumanQuery->where('jenis_hukuman_id', $cari['jenishukuman']);
        $listHukuman = $listHukumanQuery
            ->Sortable('nama_lengkap')
            ->paginate(config('bkpsdm.pagination_row_count'));
        $listJenisHukuman = JenisHukumanModel::orderBy('hukuman_id')->get();
        return view('pegawaihukuman.index', compact('listHukuman', 'listJenisHukuman', 'cari'));
    }

    public function detail($id)
    {
        $hukuman = PegawaiHukumanViewModel::where('hukuman_id', $id)->first();
        if (is_null($hukuman)) return redirect()->route('pegawaihukuman.index');
        return view('pegawaihukuman.detail', compact('hukuman'));
    }
}
