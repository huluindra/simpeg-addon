<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PegawaiHukumanViewModel extends Model
{
    use Sortable;

    protected $table = 'view_pegawai_hukuman_disiplin';
}
