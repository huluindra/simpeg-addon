<?php

namespace App\Http\Controllers;

use App\Models\PegawaiPensiunViewModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PegawaiPensiunController extends Controller
{
    public function index(Request $request)
    {
        $bulan = $request->query('bulan', 12);
        $listPensiunAll = PegawaiPensiunViewModel::sortable('nama_lengkap')->get();
        $listPensiun = $listPensiunAll->reject(function ($item) use ($bulan) {
            $umurPensiun = 58;
            switch ($item->jenis_jabatan_id) {
                case 4:
                case 9:
                    $umurPensiun = 60;
                    break;
            }
            $tglLahirOri = Carbon::parse($item->tanggal_lahir);
            $tglLahirMod = $tglLahirOri->subMonths($bulan);
            return $tglLahirMod->age < $umurPensiun;
        });
        return view('pegawaipensiun.index', compact('listPensiun', 'bulan'));
    }
}
