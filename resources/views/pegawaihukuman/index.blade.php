@extends('layout.utama')

@section('title')
    Daftar Hukuman Disiplin
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="mb-3">
                <a href="#" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#modalFilter"><i
                        class="fa-solid fa-filter"></i> Saring</a>
                <span class="text-muted btn btn-sm" disabled="true">
                    @if (count($listHukuman))
                        {{ $listHukuman->total() }} data.
                    @else
                        0 data.
                    @endif
                    @if (!is_null($cari['nama']) || !is_null($cari['nip']) || !is_null($cari['jenishukuman']))
                        <strong>Tersaring.</strong>
                        <a href="{{ route('pegawaihukuman.index') }}">(reset)</a>
                    @endif
                </span>
            </div>
            <div class="table-responsive">
                <table class="table table-index">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>@sortablelink('nama_lengkap', 'Nama Lengkap')</th>
                            <th>@sortablelink('nama_hukuman', 'Hukuman')</th>
                            <th>@sortablelink('tanggal_sk', 'Tanggal SK')</th>
                            <th class="col-btn1"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($listHukuman) < 1)
                            <tr>
                                <td colspan="5" class="bg-warning">[tidak ada data]</td>
                            </tr>
                        @else
                            @foreach ($listHukuman as $index => $item)
                                <tr>
                                    <td>{{ $listHukuman->firstItem() + $loop->index }}</td>
                                    <td>{{ $item->nama_lengkap }}</td>
                                    <td>{{ $item->nama_hukuman }} ({{ $item->kategori_hukuman }})</td>
                                    <td>{{ AppHelper::showTanggal($item->tanggal_sk) }}</td>
                                    <td class="col-action">
                                        <a href="{{ route('pegawaihukuman.detail', $item->hukuman_id) }}" class=""
                                            title="lihat detail"><i class="fa-solid fa-circle-info"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div>
                {!! $listHukuman->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" id="modalFilter">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Saring Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <form action="" method="get" id="modalFilterForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">NIP:</label>
                            <input type="text" class="form-control" id="carinip" name="cari[nip]"
                                value="{{ $cari['nip'] }}">
                        </div>
                        <div class="form-group">
                            <label for="">Nama:</label>
                            <input type="text" class="form-control" name="cari[nama]" value="{{ $cari['nama'] }}">
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Hukuman:</label>
                            <select class="form-control" name="cari[jenishukuman]">
                                <option value=""></option>
                                @foreach ($listJenisHukuman as $item)
                                    <option value="{{ $item->hukuman_id }}"
                                        @if ($item->hukuman_id == $cari['jenishukuman']) selected="true" @endif>
                                        {{ $item->nama_hukuman }}
                                        ({{ $item->kategori_hukuman }})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-outline-secondary" data-dismiss="modal">Batal</a>
                        <a href="{{ route('pegawaihukuman.index') }}" class="btn btn-secondary">Reset</a>
                        <button type="submit" class="btn btn-primary">Saring</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#modalFilter').on('shown.bs.modal', function() {
            $('#modalFilterForm #carinip').focus().select();
        });
    </script>
@endpush
